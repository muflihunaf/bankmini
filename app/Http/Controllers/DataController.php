<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Data;
use DB;

class DataController extends Controller
{
    public function index()
    {
        // $users = User::orderBy('id','DESC')->where("is_admin","=","0")->paginate(5);
        $data = DB::table('users')
        ->select('users.*','data.*')
        ->join('data','data.user_id','=','users.id')
        ->where("users.is_admin","=","0")
        ->orderBy('users.id','DESC')
        ->paginate(10);
        return view('data.home', compact('users','data'));
    }

    public function edit($id)
    {
        $kita = Data::find($id);
        $data = User::find($id);
        return view('/data/edit' , compact('data','kita') );
    }

    public function update(Request $request, $id)
    {
        $result = Data::find($id);
        if (empty($result)) {
        $data = new Data;
        $data->kelas = $request->kelas;
        $data->alamat = $request->alamat;
        $data->user_id = $request->user_id;
        $data->save();
        }else{
        $data = Data::find($id);
        $data->kelas = $request->kelas;
        $data->alamat = $request->alamat;
        $data->user_id = $request->user_id;
        $data->save();
        }

        return redirect('data/home')->withInfo('Berhasil Update Data');
    }
    public function destroy($id)
    {
        $data = User::find($id);
        $data->delete();
        return redirect('/data/home')->withDanger('Berhasil Hapus Data');
        
    }

}
