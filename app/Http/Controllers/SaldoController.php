<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Data;
use DB;

class SaldoController extends Controller
{
    public function tsaldo($id)
    {
        $data = User::find($id);
        return view('/data/tsaldo', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'saldo' => 'required|Min:1000|Numeric',
        ]);

        $data = Data::find($id);
        $data->saldo = $request->saldo + $data->saldo;
        $data->save();
        return redirect('/data/home')->withsuccess('Berhasil Menambahkan saldo');
        
    }
    public function show($id)
    {
    	$kita = DB::table('users')
        ->select('users.*','data.*')
        ->join('data','data.user_id','=','users.id')
        ->where('data.user_id','=',$id)
        ->get();
    	
    	return view('/data/lihat', ['kita' => $kita]);
    }
}
