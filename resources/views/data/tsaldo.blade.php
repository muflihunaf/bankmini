@extends('layouts.app')

@section('title', 'Tambah Saldo')

@section('content')
@if(Auth::user()->is_admin == 1)
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tambah Saldo</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/data/{{ $data->id }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('saldo') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Nominal</label>
                            <input type="hidden" name="_method" value="PUT">
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="saldo" value="" required autofocus>

                                @if ($errors->has('saldo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('saldo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@else
    @include('layouts.404')    
@endif
@endsection
