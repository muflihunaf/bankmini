@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Lihat User</div>

                <div class="panel-body">
                    <form class="form-horizontal">
                        {{ csrf_field() }}

                        
                        @foreach($kita as $data)
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nama</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ $data->name }}" disabled >
                        </div>
						</div>
                        <div class="form-group">
                            <label  class="col-md-4 control-label">Kelas</label>

                            <div class="col-md-6">
                                <input type="text" name="" value="{{ $data->kelas }}" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Alamat</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="alamat" disabled value=" {{ $data->alamat }} ">
                            </div>
                        </div>
                        <div class="form-group">
                        	<label class="col-md-4 control-label">Saldo</label>

                        	<div class="col-md-6">
                        		<input type="text" class="form-control" disabled value=" {{$data->saldo}} ">
                        	</div>
                        </div>
                        @endforeach
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
