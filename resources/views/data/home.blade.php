@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}

                        </div>
                    @endif

                    @if(Auth::user()->is_admin == 1)
                        <div class="">
                            <table class="table table-bordered">
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>kelas</th>
                                     <th>Alamat</th>
                                    <th colspan="5" class="text-center">Option</th>
                                </tr>
                                <?php $no = 1; ?>
                                 @foreach($data as $user)
                                    <tr>
                                        <td>{{ $no++ }} </td>
                                        <td> {{ $user->name}} </td>
                                        <td> {{ $user->kelas}} </td>
                                        <td> {{ $user->alamat}} </td>
                                        <td><a href="/data/{{ $user->id}}/lihat " class="btn btn-primary">Lihat tabungan</a></td>
                                        <td><a href="/data/{{ $user->id }}/edit " class="btn btn-info">Edit</a></td>
                                        <td><a href="/data/{{ $user->id }}/delete " class="btn btn-danger">delete</a></td>
                                        <td><a href="/data/{{ $user->id }}/tambah " class="btn btn-primary">Tambah</a></td>

                                    </tr>
                                @endforeach
                                
                            </table>
                            {{ $data->links() }}
                        </div>
                    @else

                    <div class="panel-body">
                        <p>Welcome User</p>
                        
                    </div>

                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
