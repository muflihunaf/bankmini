@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Isi Data Diri</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/data/{{ $data->id }}/user">
                        {{ csrf_field() }}

                        <input type="hidden" name="_method" value=" PUT ">

                        <input type="hidden" name="user_id" value="{{ $data->id}} ">

                        <div class="form-group{{ $errors->has('kelas') ? ' has-error' : '' }}">
                            <label  class="col-md-4 control-label">Kelas</label>
                            
                            
                            <div class="col-md-6">
                                <select name="kelas" id="kelas" class="form-control" required>
                                    <option value="{{ $kita['kelas'] }}">{{ $kita['kelas'] }} </option>
                                    <option value="X">X</option>
                                    <option value="XI">XI</option>
                                    <option value="XII">XII</option>
                                </select>

                                @if ($errors->has('kelas'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kelas') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Alamat</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="alamat" required value=" {{ $kita['alamat'] }} ">
                                @if ($errors->has('kelas'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('kelas') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
