<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::middleware('auth')->group(function(){

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/data/home', 'DataController@index');
Route::get('/data/{id}/edit', 'DataController@edit' );
Route::put('/data/{id}/user', 'DataController@update' );
Route::get('/data/{id}/delete', 'DataController@destroy' );
Route::get('/data/{id}/tambah', 'SaldoController@tsaldo' );
Route::put('/data/{id}', 'SaldoController@update' );
Route::get('data/{id}/lihat', 'SaldoController@show');

});